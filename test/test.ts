import { expect, tap } from '@pushrocks/tapbundle';
import * as detector from '../ts/index.js';

let testDetector: detector.Detector;

tap.test('first test', async () => {
  testDetector = new detector.Detector();
  expect(testDetector).toBeInstanceOf(detector.Detector);
});

tap.test('should detect an closed port on a local domain', async () => {
  const result = await testDetector.isActive('http://localhost:3008');
  expect(result).toBeFalse();
});

tap.test('should detect an open port on a remote domain', async () => {
  const result = await testDetector.isActive('https://lossless.com');
  expect(result).toBeTrue();
});

tap.start();
